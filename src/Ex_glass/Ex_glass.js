import React, { Component } from "react";
import { dataGlass } from "./dataGlass";
export default class Ex_glass extends Component {
  state = {
    image: "/glassesImage/v1.png",
  };

  handleChangeGlasses = (url) => {
    this.setState({
      image: url,
    });
  };

  renderClickButton = (dataArr) => {
    return dataArr.map((item) => {
      return (
        <div
          onClick={() => {
            this.handleChangeGlasses(item.url);
          }}
          className="col-2"
        >
          <img className="w-100" src={item.url} alt="" />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="page_body">
        <div className="container">
          <h2>Try Glass App Online</h2>
          <div className="mt-5">
            <div className="model">
              <img
                style={{ width: "200px" }}
                src="/glassesImage/model.jpg"
                alt=""
              />
              <img className="model_glass" src={this.state.image} alt="" />
            </div>
          </div>
          {/* <div className="row">
          <div className="col-2">
            <img className="w-100" src="/glassesImage/v1.png" alt="" />
          </div>
          <div className="col-2">
            <img className="w-100" src="/glassesImage/v2.png" alt="" />
          </div>
          <div className="col-2">
            <img className="w-100" src="/glassesImage/v3.png" alt="" />
          </div>
          <div className="col-2">
            <img className="w-100" src="/glassesImage/v4.png" alt="" />
          </div>
          <div className="col-2">
            <img className="w-100" src="/glassesImage/v5.png" alt="" />
          </div>
          <div className="col-2">
            <img className="w-100" src="/glassesImage/v6.png" alt="" />
          </div>
          <div className="col-2">
            <img className="w-100" src="/glassesImage/v7.png" alt="" />
          </div>
          <div className="col-2">
            <img className="w-100" src="/glassesImage/v8.png" alt="" />
          </div>
          <div className="col-2">
            <img className="w-100" src="/glassesImage/v9.png" alt="" />
          </div>
        </div> */}

          <div className="row my-5">{this.renderClickButton(dataGlass)}</div>
        </div>
      </div>
    );
  }
}
// https://movienew.cybersoft.edu.vn/hinhanh/trainwreck.jpg
